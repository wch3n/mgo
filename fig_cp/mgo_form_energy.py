#!/usr/bin/env python

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pylab as plt
from matplotlib import rc
from matplotlib.ticker import MultipleLocator

rigid_1 = np.genfromtxt('rigid_1+.dat')
relax_1 = np.genfromtxt('relax_1+.dat')
relax_2 = np.genfromtxt('relax_2+.dat')

a = 4.26

rigid_1[:,0] = 1./(rigid_1[:,0]*a/10)
relax_1[:,0] = 1./(relax_1[:,0]*a/10)
relax_2[:,0] = 1./(relax_2[:,0]*a/10)

xx = np.arange(0., 1.3, 0.01)
ones = np.ones(xx.size)

def func(x,a,b,c):
    return a*x+b*x**3+c

def fit(funct, target):
    popt, pcov = curve_fit(funct,target[:,0], target[:,1])
    return popt
    
p_rigid_1 = fit(func, rigid_1)
p_relax_1 = fit(func, relax_1)
p_relax_2 = fit(func, relax_2)

xlabel = '$1/L$ ($\\times$ 10$^\mathregular{-1}$ \AA$^\mathregular{-1}$)'

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
fig_width = 17/2.54  
fig_height = fig_width*0.4
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 7,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'xtick.major.size': 2,
          'xtick.minor.size': 1,
          'ytick.major.size': 2,
          'ytick.minor.size': 1,                
          'text.usetex': False,
          'figure.figsize': fig_size,
          'lines.markersize': 4}
plt.rcParams.update(params)

labelx = -0.15
crimson = "#DC143C"
dodger = "#1E90FF"

fig, (ax0, ax1, ax2) = plt.subplots(1,3)

natom = [64, 216, 512, 1000]

ax0.set_ylim([3.0,6.0])
ax0.set_xlim([0.0,1.3])
ax0.set_ylabel('Formation energy (eV)')
ax0.set_xlabel(r'$1/L$ ($\times 10^{-1} \AA^{-1}$)')
ax0.plot(rigid_1[:,0], rigid_1[:,1],'o', label='conventional', zorder = 10, lw = 0.8)
ax0.plot(rigid_1[:,0], rigid_1[:,2],'-s',label='conventional corrected', zorder = 10, lw = 0.8)
ax0.plot(rigid_1[:,0], rigid_1[:,3],'->',label='charge compensated', zorder = 10, lw = 0.8)
ax0.yaxis.set_minor_locator(MultipleLocator(0.1))
ax0.xaxis.set_major_locator(MultipleLocator(0.2))
ax0.xaxis.set_minor_locator(MultipleLocator(0.1))
ax0.legend(loc='lower left')
ax0.text(0.1, 0.85, r'$F^+$ rigid', transform=ax0.transAxes, fontsize=8)
ax0.text(0.6, 0.85, r'oxygen rich, $\mu = E_{v}$', transform=ax0.transAxes, fontsize=6)
ax0.plot(xx, func(xx,p_rigid_1[0], p_rigid_1[1], p_rigid_1[2]), '-', zorder = 1, lw = 0.8)
[i.set_linewidth(0.5) for i in ax0.spines.itervalues()]
for X, Y, Z in zip(rigid_1[:,0], rigid_1[:,1], natom):
    ax0.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(-5,-15), ha='left', textcoords='offset points', fontsize=6)

ax1.set_xlabel(r'$1/L$ ($\times 10^{-1} \AA^{-1}$)')
ax1.set_ylim([3.6,5.0])
ax1.set_xlim([0.0,1.3])
ax1.plot(relax_1[:,0], relax_1[:,1],'o', zorder = 10, lw = 0.8)
ax1.plot(relax_1[:,0], relax_1[:,2],'-s', zorder = 10, lw = 0.8)
ax1.plot(relax_1[:,0], relax_1[:,3],'->', zorder = 10, lw = 0.8)
ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
ax1.xaxis.set_major_locator(MultipleLocator(0.2))
ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
ax1.text(0.1, 0.85, r'$F^+$ relaxed', transform=ax1.transAxes, fontsize=8)
ax1.plot(xx, func(xx,p_relax_1[0], p_relax_1[1], p_relax_1[2]), '-', zorder = 1, lw = 0.8)
[i.set_linewidth(0.5) for i in ax1.spines.itervalues()]

ax2.set_xlabel(r'$1/L$ ($\times 10^{-1} \AA^{-1}$)')
ax2.set_ylim([0.5,4.0])
ax2.set_xlim([0.0,1.3])
ax2.plot(relax_2[:,0], relax_2[:,1],'o', zorder = 10, lw = 0.8)
ax2.plot(relax_2[:,0], relax_2[:,2],'-s', zorder = 10, lw = 0.8)
ax2.plot(relax_2[:,0], relax_2[:,3],'->', zorder = 10, lw = 0.8)
ax2.yaxis.set_minor_locator(MultipleLocator(0.1))
ax2.xaxis.set_major_locator(MultipleLocator(0.2))
ax2.xaxis.set_minor_locator(MultipleLocator(0.1))
ax2.text(0.1, 0.85, r'$F^{2+}$ relaxed', transform=ax2.transAxes, fontsize=8)
ax2.plot(xx, func(xx,p_relax_2[0], p_relax_2[1], p_relax_2[2]), '-', zorder = 1, lw = 0.8)
[i.set_linewidth(0.5) for i in ax2.spines.itervalues()]

plt.tight_layout(pad=0.3)

plt.savefig('mgo_form_energy.pdf')
plt.show()

#!/usr/bin/env python

import numpy as np
import matplotlib.pylab as plt
from matplotlib import rc
from matplotlib.ticker import MultipleLocator, NullLocator

dat = np.genfromtxt('ctl.dat')

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
fig_width = 8.5/2.54  
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'figure.figsize': fig_size,
          'lines.markersize': 4}
plt.rcParams.update(params)

crimson = "#DC143C"
dodger = "#1E90FF"


x = np.linspace(0,5,10001)

def energy(x, dat):
    return np.piecewise(x, [(x>=0) & (x<=1), (x>=1) & (x<=2), (x>=2) & (x<=3), \
                 (x>=3) & (x<=4), (x>=4)],[dat[i] for i in xrange(5)])

vbm = energy(x,dat[:,1])
cbm = energy(x,dat[:,2])


fig, ax0 = plt.subplots(1,1)

ax0.set_xlim([0.0,5.0])

ax0.plot(x, vbm, '-', x, cbm, '-', linewidth=1, color='k', solid_joinstyle='round')

h = 0.2
for i in xrange(5):
        ax0.hlines(dat[i,3], i+h, i+1-h, color=crimson, linewidth=0.8)
        ax0.errorbar(i+0.5, dat[i,3], yerr= dat[i,4], linewidth=0.8, marker='.', \
                     mec='k', mfc=crimson, ecolor='k', mew= 0.1)


ax0.set_ylabel(r'Energy (eV)')
ax0.yaxis.set_minor_locator(MultipleLocator(0.5))
ax0.yaxis.set_major_locator(MultipleLocator(1))
ax0.xaxis.set_major_locator(NullLocator())
ax0.tick_params('y', length=2, width=0.5, which='major')
ax0.tick_params('y', length=1, width=0.5, which='minor')

[i.set_linewidth(0.5) for i in ax0.spines.itervalues()]


method = ['PBE', 'PBE0', '${G_0W_0}$', \
          '${G_0W_0}$@PBE0', 'QS${GW}$']
ind = np.arange(5)

[ax0.text(ind[i]+0.5, 7.3, method[i], fontsize=6, va='bottom', ha='center') for i in ind]

plt.tight_layout()
plt.savefig('ctl.eps')
plt.show()

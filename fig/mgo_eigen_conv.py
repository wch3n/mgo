#!/usr/bin/env python

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pylab as plt
from matplotlib import rc
from matplotlib.ticker import MultipleLocator

dat = np.genfromtxt('eigen_conv.dat')
dat[:,1] = (1./dat[:,1]*1000)**(1./3)

xx = np.arange(0., 1, 0.01)
ones = np.ones(xx.size)

dat0, dat1, dat2, dat3 = np.split(dat,4)

def func(x,a,b,c):
    return a*x+b*x**3+c

def fit(funct, a):
    popt0, pcov = curve_fit(funct,a[:,1], a[:,3])
    popt1, pcov = curve_fit(funct,a[:,1], a[:,3]-a[:,5])
    popt2, pcov = curve_fit(funct,a[:,1], a[:,4])
    popt3, pcov = curve_fit(funct,a[:,1], a[:,3]-a[:,2])
    popt4, pcov = curve_fit(funct,a[:,1], a[:,4]-a[:,2])
    return popt0, popt1, popt2, popt3, popt4
    
popt10, popt11, popt12, popt13, popt14 = fit(func, dat1[:4,:])
popt20, popt21, popt22, popt23, popt24 = fit(func, dat2[:4,:])
popt30, popt31, popt32, popt33, popt34 = fit(func, dat3[:4,:])

xlabel = '$\Omega^{-1/3}$ ($\\times$ 10$^\mathregular{-1}$ bohr$^\mathregular{-1}$)'


rc('font',**{'family':'sans-serif','sans-serif':['Helvetica Neue']})
fig_width = 17/2.54  
fig_height = fig_width*0.7
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'xtick.major.size': 2,
          'xtick.minor.size': 1,
          'ytick.major.size': 2,
          'ytick.minor.size': 1,                
          'text.usetex': False,
          'figure.figsize': fig_size,
          'lines.markersize': 4}
plt.rcParams.update(params)

labelx = -0.15
crimson = "#DC143C"
dodger = "#1E90FF"

fig, ((ax0, ax3, ax6, ax9), (ax1, ax4, ax7, ax10), (ax2, ax5, ax8, ax11)) = plt.subplots(3,4, sharex = 'col')
ax0.set_ylim([10.0,11.0])
ax0.set_xlim([0.0,0.85])
ax0.set_ylabel(r'$a_{1g}$ (eV)')
ax0.plot(dat0[:,1], dat0[:,3],'-.o', mec=crimson, color=crimson, zorder = 10, lw = 0.8)
ax0.plot(dat0[:,1], dat0[:,3]-dat0[:,5],'-.o', mfc='w', mec=crimson, color=crimson, zorder = 9, lw= 0.8)
ax0.yaxis.set_minor_locator(MultipleLocator(0.1))
ax0.xaxis.set_major_locator(MultipleLocator(0.2))
ax0.xaxis.set_minor_locator(MultipleLocator(0.1))
ax0.text(0.1, 0.85, r'$q=0@\mathbf{R}_0$', transform=ax0.transAxes, fontsize=8)
[i.set_linewidth(0.5) for i in ax0.spines.itervalues()]


ax1.set_xlim([0.0,0.85])
ax1.set_ylim([7.5,8.5])
ax1.set_ylabel(r'VBM (eV)')
ax1.plot(dat0[:,1], dat0[:,2],'-.o', mec=crimson, color=crimson, zorder = 10, lw = 0.8)
ax1.plot(dat0[:,1], dat0[:,2]-dat0[:,5],'-.o', mfc='w', mec=crimson, color=crimson, zorder = 9, lw= 0.8)
ax1.plot(xx, 8.0*ones, ':', color=crimson, zorder = 1, lw = 0.8)
ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
ax1.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax1.spines.itervalues()]

ax2.set_ylim([2.0,3.0])
ax2.set_xlim([0.0,0.85])
ax2.set_ylabel(r'$a_{1g}$-VBM (eV)')
ax2.set_xlabel(xlabel)
ax2.plot(dat0[:,1], dat0[:,3]-dat0[:,2],'-.o', mec=crimson, color=crimson, zorder = 10, lw = 0.8)
ax2.yaxis.set_minor_locator(MultipleLocator(0.1))
ax2.xaxis.set_minor_locator(MultipleLocator(0.1))
ax2.xaxis.set_major_locator(MultipleLocator(0.2))
ax2.tick_params('both', length=2, width=0.5, which='major')
ax2.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax2.spines.itervalues()]
for X, Y, Z in zip(dat0[:,1], dat0[:,3]-dat0[:,2], dat0[:,0]):
    ax2.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,10), ha='center', textcoords='offset points', fontsize=4, color=crimson)


ax3.set_ylim([8.4,10.6])
ax3.set_xlim([0.0,0.85])
ax3.plot(dat1[:,1], dat1[:,3],'o', mec=crimson, color=crimson, zorder = 10)
ax3.plot(dat1[:,1], dat1[:,3]-dat1[:,5],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax3.plot(dat1[:,1], dat1[:,4],'s', mec='g', color='g', zorder = 10)
ax3.plot(dat1[:,1], dat1[:,4]-dat1[:,5],'s', mfc='w',mec='g', color='g', zorder = 9)
ax3.plot(xx, func(xx,popt10[0], popt10[1], popt10[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax3.plot(xx, func(xx,popt11[0], popt11[1], popt11[2]), '-', color=crimson, zorder = 1, lw = 0.8)
#ax3.plot(xx, func(xx,popt12[0], popt12[1], popt12[2]), '-', color='g', zorder = 1, lw = 0.8)
ax3.plot(xx, popt10[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax3.yaxis.set_minor_locator(MultipleLocator(0.25))
ax3.xaxis.set_minor_locator(MultipleLocator(0.1))
ax3.xaxis.set_major_locator(MultipleLocator(0.2))
ax3.text(0.1, 0.85, r'$q=+1@\mathbf{R}_0$', transform=ax3.transAxes, fontsize=8)
[i.set_linewidth(0.5) for i in ax3.spines.itervalues()]


ax4.set_xlim([0.0,0.85])
ax4.set_ylim([7.5,8.5])
ax4.plot(dat1[:,1], dat1[:,2],'-.o', mec=crimson, color=crimson, zorder = 10, lw = 0.8)
ax4.plot(dat1[:,1], dat1[:,2]-dat1[:,5],'-.o', mfc='w', mec=crimson, color=crimson, zorder = 9, lw = 0.8)
ax4.plot(xx, 8.0*ones, ':', color=crimson, zorder = 1, lw = 0.8)
ax4.yaxis.set_minor_locator(MultipleLocator(0.1))
ax4.xaxis.set_minor_locator(MultipleLocator(0.1))
ax4.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax4.spines.itervalues()]


ax5.set_ylim([0.5,3.0])
ax5.set_xlim([0.0,0.85])
ax5.set_xlabel(xlabel)
ax5.plot(dat1[:,1], dat1[:,3]-dat1[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax5.plot(dat1[:,1], dat1[:,4]-dat1[:,2],'s', mec='g', color='g', zorder = 10)
ax5.plot(xx, func(xx,popt13[0], popt13[1], popt13[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax5.plot(xx, popt13[2]*ones, 'g:', zorder = 1, lw = 0.8)
#ax5.plot(xx, func(xx,popt14[0], popt14[1], popt14[2]), 'g-', zorder = 1, lw = 0.8)
ax5.yaxis.set_minor_locator(MultipleLocator(0.25))
ax5.xaxis.set_minor_locator(MultipleLocator(0.1))
ax5.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax5.spines.itervalues()]
for X, Y, Z in zip(dat1[:,1], dat1[:,3]-dat1[:,2], dat1[:,0]):
    ax5.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,10), ha='center', textcoords='offset points', fontsize=4, color=crimson)


ax6.set_ylim([8.5,10.5])
ax6.set_xlim([0.0,0.85])
ax6.plot(dat2[:,1], dat2[:,3],'o', mec=crimson, color=crimson, zorder = 10)
ax6.plot(dat2[:,1], dat2[:,3]-dat2[:,5],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax6.plot(dat2[:,1], dat2[:,4],'s', mec='g', color='g', zorder = 10)
ax6.plot(dat2[:,1], dat2[:,4]-dat2[:,5],'s', mfc='w', mec='g', color='g', zorder = 9)
ax6.plot(xx, func(xx,popt20[0], popt20[1], popt20[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax6.plot(xx, func(xx,popt21[0], popt21[1], popt21[2]), '-', color=crimson, zorder = 1, lw = 0.8)
#ax6.plot(xx, func(xx,popt22[0], popt22[1], popt22[2]), '-', color='g', zorder = 1, lw = 0.8)
ax6.plot(xx, popt20[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax6.yaxis.set_minor_locator(MultipleLocator(0.25))
ax6.xaxis.set_minor_locator(MultipleLocator(0.1))
ax6.xaxis.set_major_locator(MultipleLocator(0.2))
ax6.text(0.1, 0.85, r'$q=+1@\mathbf{R}_+$', transform=ax6.transAxes, fontsize=8)
[i.set_linewidth(0.5) for i in ax6.spines.itervalues()]


ax7.set_xlim([0.0,0.85])
ax7.set_ylim([7.5,8.5])
ax7.plot(dat2[:,1], dat2[:,2],'-.o', mec=crimson, color=crimson, zorder = 10, lw = 0.8)
ax7.plot(dat2[:,1], dat2[:,2]-dat2[:,5],'-.o', mfc='w', mec=crimson, color=crimson, zorder = 9, lw = 0.8)
ax7.plot(xx, 8.0*ones, ':', color=crimson, zorder = 1, lw = 0.8)
ax7.yaxis.set_minor_locator(MultipleLocator(0.1))
ax7.xaxis.set_minor_locator(MultipleLocator(0.1))
ax7.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax7.spines.itervalues()]


ax8.set_ylim([0.5,2.5])
ax8.set_xlim([0.0,0.85])
ax8.set_xlabel(xlabel)
ax8.plot(dat2[:,1], dat2[:,3]-dat2[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax8.plot(dat2[:,1], dat2[:,4]-dat2[:,2],'s', mec='g', color='g', zorder = 10)
ax8.plot(xx, func(xx,popt23[0], popt23[1], popt23[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax8.plot(xx, popt23[2]*ones, 'g:', zorder =1, lw = 0.8)
#ax8.plot(xx, func(xx,popt24[0], popt24[1], popt24[2]), 'g-', zorder = 1, lw = 0.8)
ax8.yaxis.set_minor_locator(MultipleLocator(0.25))
ax8.xaxis.set_minor_locator(MultipleLocator(0.1))
ax8.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax8.spines.itervalues()]
for X, Y, Z in zip(dat2[:,1], dat2[:,3]-dat2[:,2], dat2[:,0]):
    ax8.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,10), ha='center', textcoords='offset points', fontsize=4, color=crimson)


ax9.set_ylim([7.6,10.4])
ax9.set_xlim([0.0,0.85])
ax9.plot(dat3[:,1], dat3[:,3],'o', mec=crimson, color=crimson, zorder = 10)
ax9.plot(dat3[:,1], dat3[:,3]-dat3[:,5],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax9.plot(dat3[:,1], dat3[:,4],'s', mec='g', color='g', zorder = 10)
ax9.plot(dat3[:,1], dat3[:,4]-dat3[:,5],'s', mfc='w', mec='g', color='g', zorder = 9)
ax9.plot(xx, func(xx,popt30[0], popt30[1], popt30[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax9.plot(xx, func(xx,popt31[0], popt31[1], popt31[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax9.plot(xx, popt30[2]*ones, 'g:', zorder =1, lw = 0.8)
#ax9.plot(xx, func(xx,popt32[0], popt32[1], popt32[2]), '-', color='g', zorder = 1, lw = 0.8)
#ax9.plot(xx, popt30[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax9.yaxis.set_minor_locator(MultipleLocator(0.25))
ax9.xaxis.set_minor_locator(MultipleLocator(0.1))
ax9.xaxis.set_major_locator(MultipleLocator(0.2))
ax9.text(0.1, 0.85, r'$q=+2@\mathbf{R}_+$', transform=ax9.transAxes, fontsize=8)
[i.set_linewidth(0.5) for i in ax9.spines.itervalues()]


ax10.set_xlim([0.0,0.85])
ax10.set_ylim([7.5,8.5])
ax10.plot(dat3[:,1], dat3[:,2],'-.o', mec=crimson, color=crimson, zorder = 10, lw = 0.8)
ax10.plot(dat3[:,1], dat3[:,2]-dat3[:,5],'-.o', mfc='w', mec=crimson, color=crimson, zorder = 9, lw = 0.8)
ax10.plot(xx, 8.0*ones, ':', color=crimson, zorder = 1, lw = 0.8)
ax10.yaxis.set_minor_locator(MultipleLocator(0.1))
ax10.xaxis.set_minor_locator(MultipleLocator(0.1))
ax10.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax10.spines.itervalues()]

ax11.set_ylim([-0.4,2.4])
ax11.set_xlim([0.0,0.85])
ax11.set_xlabel(xlabel)
ax11.plot(dat3[:,1], dat3[:,3]-dat3[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax11.plot(dat3[:,1], dat3[:,4]-dat3[:,2],'s', mec='g', color='g', zorder = 10)
ax11.plot(xx, func(xx,popt33[0], popt33[1], popt33[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax11.plot(xx, popt33[2]*ones, 'g:', zorder =1, lw = 0.8)
#ax11.plot(xx, func(xx,popt34[0], popt34[1], popt34[2]), 'g-', zorder = 1, lw = 0.8)
ax11.yaxis.set_minor_locator(MultipleLocator(0.25))
ax11.xaxis.set_minor_locator(MultipleLocator(0.1))
ax11.xaxis.set_major_locator(MultipleLocator(0.2))
[i.set_linewidth(0.5) for i in ax11.spines.itervalues()]
for X, Y, Z in zip(dat3[:,1], dat3[:,3]-dat3[:,2], dat3[:,0]):
    ax11.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,10), ha='center', textcoords='offset points', fontsize=4, color=crimson)

plt.tight_layout(pad=0.3)

plt.savefig('mgo_eigen_conv.pdf')
plt.show()

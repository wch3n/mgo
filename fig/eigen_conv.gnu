#!/usr/bin/env gnuplot

set terminal postscript eps enhanced color font "Helvetica, 15" size 17.5cm, 10.cm
set output "eigen_conv.eps"
set size square
set border lc rgb "black" lw 0.7
set boxwidth 1.0 absolute

set ytics font ",12"
set xtics font ",12"
set format y "%3.1f"
set ytics scale 0.5
set mytics 2
set xtics 0.2 nomirror
set mxtics 2
set pointsize 1.2

set style line 1 lt 1 lc rgb "#DC143C" lw 2
set style line 2 lt 2 lc rgb "#DC143C" lw 2
set style line 3 lt 1 lc rgb "navy" lw 2
set style line 4 lt 2 lc rgb "navy" lw 2

set tmargin 0.0
set bmargin 0.0
set lmargin 6.5
set rmargin 1.0
set format x ""
unset key

set xrange [0.0:0.9]

set multiplot layout 3,4 columnsfirst

# rigid 0
set title "{/Helvetica-Italic q}=0\\@R_0" offset 0,-0.5
set ylabel "{/Helvetica-Italic a}_{1g} (eV)" offset 1.5,0
set yrange [10.0:10.9]
set ytics 0.2
set mytics 2
plot "eigen_conv.dat" i 0 u (1/$2*1000)**(1./3.):($4) t "raw" w lp ls 1 pt 5, \
     "eigen_conv.dat" i 0 u (1/$2*1000)**(1./3.):($4-$6) t "raw" w lp ls 2 pt 4

unset title
set ylabel "VBM (eV)" offset 1,0
set yrange[7.5:8.5]
set ytics 0.2
plot 8.0 w l lc rgb "orange" lw 2 lt 3,\
    "eigen_conv.dat" i 0 u (1/$2*1000)**(1./3.):($3) w lp ls 1 pt 5, \
     "eigen_conv.dat" i 0 u (1/$2*1000)**(1./3.):($3-$6) w lp ls 2 pt 4

unset title
set ylabel "{/Helvetica-Italic a}_{1g}-VBM (eV)" offset 1,0
set xtics
set format x "%3.1f"
set yrange [2.0:3.0]
set ytics 0.2
set xlabel "1/{/Helvetica-Italic L} (10^{-1} bohr^{-1})"
set yrange [2.0:3.0]
plot "eigen_conv.dat" i 0 u (1/$2*1000)**(1./3.):($4-$3) w lp ls 1 pt 5,\
     "eigen_conv.dat" i 0 u (1/$2*1000)**(1./3.):($4-$3):1 w labels offset -0.2,1 font ",12" tc rgb "#DC143C"

# rigid 1+ 
set title "{/Helvetica-Italic q}=+1\\@R_0 " offset 0,-0.5
set yrange [8.4:10.6]
set key at 1.92, 10.6
set key reverse Left
set key font ",12"
set key samplen 0.5
unset key
set ytics 0.4
set xtics
set ylabel " "
unset xlabel
set format x ""
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
fit f(x) "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4-$6) via a,b,c
fit g(x) "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4) via m,n,p

plot f(x) not w l lc rgb "orange" lw 2 lt 3,\
     g(x) not w l lc rgb "orange" lw 2 lt 3,\
     n not w l lc rgb "orange" lw 2 lt 3,\
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4) t "raw" w lp ls 1 pt 5, \
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4-$6) t "pot aligned" w lp ls 2 pt 4, \
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($5) t "electrostatic corr." w lp ls 3 pt 7, \
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($5-$6) t "electrostatic+aligned" w lp ls 4 pt 6

unset title
set yrange [7.5:8.5]
set ytics 0.2
set ylabel ""
plot 8.0 w l lc rgb "orange" lw 2 lt 3,\
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($3) w lp ls 1 pt 5, \
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($3-$6) w lp ls 2 pt 4

unset title
set yrange [0.5:3.0]
set ytics 0.4
set format x "%3.1f"
set xlabel "1/{/Helvetica-Italic L} (10^{-1} bohr^{-1})"
f(x) = a*x + c*x**3 + b
fit f(x) "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4-$3) via a,b,c
plot f(x) w l lc rgb "orange" lw 2 lt 3,\
     b w l lc rgb "orange" lw 2 lt 3,\
    "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4-$3) w lp ls 1 pt 5,\
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($5-$3) w lp ls 3 pt 7,\
     "eigen_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4-$3):1 w labels offset -0.2,1 font ",12" tc rgb "#DC143C"

# 1+ relaxed
set title "{/Helvetica-Italic q}=+1\\@R_{+1}" offset 0,-0.5
set yrange [8.4:10.2]
set ytics 0.4
set xtics
unset xlabel
set format x ""
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
h(x) = j*x+k*x**3+l
fit f(x) "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4-$6) via a,b,c
fit g(x) "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4) via m,n,p
fit h(x) "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($5) via j,k,l

plot f(x) not w l lc rgb "orange" lw 2 lt 3,\
     g(x) not w l lc rgb "orange" lw 2 lt 3,\
     h(x) not w l lc rgb "#9ACD32" lw 2 lt 3,\
     n not w l lc rgb "orange" lw 2 lt 3,\
     "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4) t "raw" w lp ls 1 pt 5, \
     "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4-$6) t "pot aligned" w lp ls 2 pt 4, \
     "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($5) t "electrostatic corr." w lp ls 3 pt 7, \
     "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($5-$6) t "electrostatic+aligned" w lp ls 4 pt 6


unset title
set yrange [7.5:8.5]
set ytics 0.2
plot 8.0 w l lc rgb "orange" lw 2 lt 3,\
     "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($3) w lp ls 1 pt 5, \
     "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($3-$6) w lp ls 2 pt 4

unset title
set xlabel "1/{/Helvetica-Italic L} (10^{-1} bohr^{-1})"
set yrange [0.5:3.0]
set ytics 0.4
set format x "%3.1f"
f(x) = a*x + c*x**3 + b
h(x) = j*x + k*x**3 + l
fit f(x) "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4-$3) via a,b,c
fit h(x) "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($5-$3) via j,k,l
plot f(x) w l lc rgb "orange" lw 2 lt 3,\
     h(x) w l lc rgb "#9ACD32" lw 2 lt 3,\
     b w l lc rgb "orange" lw 2 lt 3,\
    "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4-$3) w lp ls 1 pt 5,\
    "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($5-$3) w lp ls 3 pt 7,\
    "eigen_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4-$3):1 w labels offset -0.2,1 font ",12" tc rgb "#DC143C"

# 2+@1+ relaxed
set title "{/Helvetica-Italic q}=+2\\@R_{+1}" offset 0,-0.5
unset xlabel
set yrange [7.6:10.2]
set format x ""
set xtics
set ytics 0.4
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
fit f(x) "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4-$6) via a,b,c
fit g(x) "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4) via m,n,p
plot f(x) not w l lc rgb "orange" lw 2 lt 3,\
     g(x) not w l lc rgb "orange" lw 2 lt 3,\
     n not w l lc rgb "orange" lw 2 lt 3,\
     "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4) t "raw" w lp ls 1 pt 5, \
     "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4-$6) t "pot aligned" w lp ls 2 pt 4, \
     "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($5) t "electrostatic corr." w lp ls 3 pt 7, \
     "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($5-$6) t "electrostatic+aligned" w lp ls 4 pt 6

unset title
set yrange [7.5:8.5]
set ytics 0.2
plot 8.0 w l lc rgb "orange" lw 2 lt 3,\
     "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($3) w lp ls 1 pt 5, \
     "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($3-$6) w lp ls 2 pt 4

unset title
set autoscale y
set ytics 0.4
set xlabel "1/{/Helvetica-Italic L} (10^{-1} bohr^{-1})"
set format x "%3.1f"
f(x) = a*x + c*x**3 + b
h(x) = j*x + k*x**3 + l
fit f(x) "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4-$3) via a,b,c
fit h(x) "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($5-$3) via j,k,l
plot f(x) w l lc rgb "orange" lw 2 lt 3,\
     h(x) w l lc rgb "#9ACD32" lw 2 lt 3,\
     b w l lc rgb "orange" lw 2 lt 3,\
    "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4-$3) w lp ls 1 pt 5,\
    "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($5-$3) w lp ls 3 pt 7,\
    "eigen_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4-$3):1 w labels offset -0.2,1 font ",12" tc rgb  "#DC143C"


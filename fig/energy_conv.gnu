#!/usr/bin/env gnuplot

set terminal postscript eps enhanced color font "Helvetica, 15" size 8.5cm, 8cm
set output "energy_conv.eps"
set size square
set border lc rgb "black" lw 0.7
set boxwidth 1.0 absolute
set pointsize 1.2

set ytics font ",12"
set xtics font ",12"
set format y "%3.1f"
set ytics scale 0.5
set mytics 2
set xtics 0.2 nomirror
set mxtics 2

set tmargin 0.5
set bmargin 3
set rmargin 1
set lmargin 6

set style line 1 lt 1 lc rgb "#DC143C" lw 2
set style line 2 lt 2 lc rgb "#DC143C" lw 2
set style line 3 lt 1 lc rgb "navy" lw 2
set style line 4 lt 2 lc rgb "navy" lw 2

unset key

set xrange [0.0:0.9]

set ylabel "{/Symbol-Oblique m}(0/+)} (eV)" offset 1.5,0
set ytics 0.2
set mytics 2

set multiplot layout 2,2
unset xlabel
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
fit f(x) "energy_conv.dat" i 0 u (1/$2*1000)**(1./3.):($3) via a,b,c
fit g(x) "energy_conv.dat" i 0 u (1/$2*1000)**(1./3.):($4) via m,p,n
set label "rigid, {/Symbol-Oblique e_{/Symbol \245}}" at graph 0.1, 0.9 font ",13"

plot f(x) not w l lt 2 lw 2 lc rgb "orange",\
     g(x) not w l lt 2 lw 2 lc rgb "orange",\
     b not w l lt 2 lw 2 lc rgb "orange", \
     "energy_conv.dat" i 0 u (1/$2*1000)**(1./3.):($3) t "raw" w lp ls 1 pt 5, \
     "energy_conv.dat" i 0 u (1/$2*1000)**(1./3.):($4) t "aligned" w lp ls 2 pt 4, \
     "energy_conv.dat" i 0 u (1/$2*1000)**(1./3.):($5) t "corrected" w lp ls 3 pt 7, \
     "energy_conv.dat" i 0 u (1/$2*1000)**(1./3.):($3):1 not w labels offset 0.5,-1 font ",12" tc rgb "#DC143C"


unset label
set yrange [1.4:2.6]
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
fit f(x) "energy_conv.dat" i 1 u (1/$2*1000)**(1./3.):($3) via a,b,c
fit g(x) "energy_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4) via m,p,n
set label "relaxed, {/Symbol-Oblique e_{/Symbol \245}}" at graph 0.1, 0.9 font ",13"

plot f(x) not w l lt 2 lw 2 lc rgb "orange",\
     g(x) not w l lt 2 lw 2 lc rgb "orange",\
     b not w l lt 2 lw 2 lc rgb "orange", \
     "energy_conv.dat" i 1 u (1/$2*1000)**(1./3.):($3) t "raw" w lp ls 1 pt 5, \
     "energy_conv.dat" i 1 u (1/$2*1000)**(1./3.):($4) t "aligned" w lp ls 2 pt 4, \
     "energy_conv.dat" i 1 u (1/$2*1000)**(1./3.):($5) t "corrected" w lp ls 3 pt 7

unset label
set yrange [0.2:2.2]
set xlabel "1/{/Helvetica-Italic L} (10^{-1} bohr^{-1})"
set ytics 0.4
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
set ylabel "{/Symbol-Oblique m}(2+/+) (eV)" 
fit f(x) "energy_conv.dat" i 2 u (1/$2*1000)**(1./3.):($3) via a,b,c
fit g(x) "energy_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4) via m,p,n
set label "relaxed, {/Symbol-Oblique e}_{/Symbol 0}" at graph 0.1, 0.9 font ",13"

plot f(x) not w l lt 2 lw 2 lc rgb "orange",\
     g(x) not w l lt 2 lw 2 lc rgb "orange",\
     b not w l lt 2 lw 2 lc rgb "orange", \
     "energy_conv.dat" i 2 u (1/$2*1000)**(1./3.):($3) t "raw" w lp ls 1 pt 5, \
     "energy_conv.dat" i 2 u (1/$2*1000)**(1./3.):($4) t "aligned" w lp ls 2 pt 4, \
     "energy_conv.dat" i 2 u (1/$2*1000)**(1./3.):($5) t "corrected" w lp ls 3 pt 7

set autoscale y
f(x) = a*x+c*x**3+b
g(x) = m*x+p*x**3+n
set ylabel "{/Helvetica-Italic E}_{form} (+) (eV)"
fit f(x) "energy_conv.dat" i 3 u (1/$2*1000)**(1./3.):($3) via a,b,c
fit g(x) "energy_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4) via m,n,p
plot f(x) not w l lt 2 lw 2 lc rgb "orange",\
     g(x) not w l lt 2 lw 2 lc rgb "orange",\
    b not w l lt 2 lw 2 lc rgb "orange", \
    "energy_conv.dat" i 3 u (1/$2*1000)**(1./3.):($3) t "raw" w lp ls 1 pt 5,\
    "energy_conv.dat" i 3 u (1/$2*1000)**(1./3.):($4) t "aligned" w lp ls 2 pt 4, \
    "energy_conv.dat" i 3 u (1/$2*1000)**(1./3.):($5) t "corrected" w lp ls 3 pt 7

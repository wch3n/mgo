#!/usr/bin/env python

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pylab as plt
from matplotlib import rc
from matplotlib.ticker import MultipleLocator

dat = np.genfromtxt('energy_conv.dat')
dat[:,1] = (1./dat[:,1]*1000)**(1./3)

xx = np.arange(0., 1, 0.01)
ones = np.ones(xx.size)

def func(x,a,b,c):
    return a*x+b*x**3+c

def fit(funct, m):
    popt1, pconv = curve_fit(funct, m[:,1], m[:,2])
    popt2, pconv = curve_fit(funct, m[:,1], m[:,3])
    return popt1, popt2

dat0, dat1, dat2, dat3, dat4, dat5, dat6 = np.split(dat,7)

popt00, popt01 = fit(func, dat0[:5,:])
popt10, popt11 = fit(func, dat1[:5,:])
popt20, popt21 = fit(func, dat2[:5,:])
popt30, popt31 = fit(func, dat3[:5,:])
popt40, popt41 = fit(func, dat4[:5,:])
popt50, popt51 = fit(func, dat5[:5,:])
popt60, popt61 = fit(func, dat6[:5,:])

xlabel = '$\Omega^{-1/3}$ ($\\times$ 10$^\mathregular{-1}$ bohr$^\mathregular{-1}$)'


rc('font',**{'family':'sans-serif','sans-serif':['Helvetica Neue']})
fig_width = 8.5/2.54  
fig_height = fig_width*1.2
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'figure.figsize': fig_size,
          'lines.markersize': 4}
plt.rcParams.update(params)

labelx = -0.15
crimson = "#DC143C"
dodger = "#1E90FF"

fig, ((ax0, ax1), (ax2, ax3), (ax4, ax5)) = plt.subplots(3,2, sharex = 'col')

'''ax0.set_ylim([1.4,2.6])
ax0.set_xlim([0.0,0.9])
ax0.set_ylabel(r'$\mu(0/+)$ (eV)')
ax0.plot(dat0[:,1], dat0[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax0.plot(dat0[:,1], dat0[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax0.plot(dat0[:,1], dat0[:,4],'s', mec='g', color='g', zorder = 10)
ax0.plot(xx, func(xx,popt00[0], popt00[1], popt00[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax0.plot(xx, func(xx,popt01[0], popt01[1], popt01[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax0.plot(xx, popt00[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax0.yaxis.set_minor_locator(MultipleLocator(0.1))
ax0.xaxis.set_major_locator(MultipleLocator(0.2))
ax0.xaxis.set_minor_locator(MultipleLocator(0.1))
ax0.text(0.1, 0.85, r'rigid, $\varepsilon_\infty$', transform=ax0.transAxes, fontsize=8)
ax0.tick_params('both', length=2, width=0.5, which='major')
ax0.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax0.spines.itervalues()]
for X, Y, Z in zip(dat0[:,1], dat0[:,2], dat0[:,0]):
    ax0.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,-10), ha='left', textcoords='offset points', fontsize=6, color=crimson)
'''

ax5.set_xlim([0.0,0.9])
ax5.set_ylim([0.2,2.2])
ax5.set_xlabel(xlabel)
#ax5.set_ylabel(r'$\mu(+/2+)$ (eV)')
ax5.plot(dat2[:,1], dat2[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax5.plot(dat2[:,1], dat2[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax5.plot(dat2[:,1], dat2[:,4],'s-', mec='g', color='g', zorder = 10)
ax5.plot(xx, func(xx,popt20[0], popt20[1], popt20[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax5.plot(xx, func(xx,popt21[0], popt21[1], popt21[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax5.plot(xx, popt20[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax5.yaxis.set_minor_locator(MultipleLocator(0.25))
ax5.xaxis.set_minor_locator(MultipleLocator(0.1))
ax5.xaxis.set_major_locator(MultipleLocator(0.2))
ax5.text(0.1, 0.85, r'(+/2+)', transform=ax5.transAxes, fontsize=8)
ax5.tick_params('both', length=2, width=0.5, which='major')
ax5.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax5.spines.itervalues()]
for X, Y, Z in zip(dat2[:,1], dat2[:,2], dat2[:,0]):
    ax5.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,-10), ha='center', textcoords='offset     points', fontsize=4, color=crimson)

ax4.set_ylim([1.4,2.6])
ax4.set_xlim([0.0,0.9])
ax4.set_ylabel(r'$\mu(q/q+1)$ (eV)')
ax4.set_xlabel(xlabel)
ax4.plot(dat1[:,1], dat1[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax4.plot(dat1[:,1], dat1[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax4.plot(dat1[:,1], dat1[:,4],'s-', mec='g', color='g', zorder = 10)
ax4.plot(xx, func(xx,popt10[0], popt10[1], popt10[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax4.plot(xx, func(xx,popt11[0], popt11[1], popt11[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax4.plot(xx, popt10[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax4.yaxis.set_minor_locator(MultipleLocator(0.1))
ax4.xaxis.set_minor_locator(MultipleLocator(0.1))
ax4.xaxis.set_major_locator(MultipleLocator(0.2))
ax4.text(0.1, 0.85, r'(0/+)', transform=ax4.transAxes, fontsize=8)
ax4.tick_params('both', length=2, width=0.5, which='major')
ax4.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax4.spines.itervalues()]
for X, Y, Z in zip(dat1[:,1], dat1[:,2], dat1[:,0]):
    ax4.annotate('{}'.format(int(Z)), xy=(X,Y), xytext=(0,-10), ha='center', textcoords='offset     points', fontsize=4, color=crimson)



ax1.set_ylim([3.2,4.4])
ax1.set_xlim([0.0,0.9])
#ax1.set_ylabel(r'$E_{\rm form} (+)$ (eV)')
#ax3.set_xlabel(xlabel)
ax1.plot(dat3[:,1], dat3[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax1.plot(dat3[:,1], dat3[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax1.plot(dat3[:,1], dat3[:,4],'s-', mec='g', color='g', zorder = 10)
ax1.plot(xx, func(xx,popt30[0], popt30[1], popt30[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax1.plot(xx, func(xx,popt31[0], popt31[1], popt31[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax1.plot(xx, popt30[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
ax1.xaxis.set_major_locator(MultipleLocator(0.2))
ax1.text(0.1, 0.85, r'R$_\mathregular{+}$, $\varepsilon_0$', transform=ax1.transAxes, fontsize=8)
ax1.tick_params('both', length=2, width=0.5, which='major')
ax1.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax1.spines.itervalues()]

ax0.set_ylim([4.2,5.4])
ax0.set_xlim([0.0,0.9])
ax0.plot(dat4[:,1], dat4[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax0.set_ylabel(r'$E_{\rm form} (+)$ (eV)')
ax0.plot(dat4[:,1], dat4[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax0.plot(dat4[:,1], dat4[:,4],'s-', mec='g', color='g', zorder = 10)
ax0.plot(xx, func(xx,popt40[0], popt40[1], popt40[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax0.plot(xx, func(xx,popt41[0], popt41[1], popt41[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax0.plot(xx, popt40[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax0.yaxis.set_minor_locator(MultipleLocator(0.1))
ax0.xaxis.set_minor_locator(MultipleLocator(0.1))
ax0.xaxis.set_major_locator(MultipleLocator(0.2))
ax0.text(0.1, 0.85, r'R$_\mathregular{0}$, $\varepsilon_\infty$', transform=ax0.transAxes, fontsize=8)
ax0.tick_params('both', length=2, width=0.5, which='major')
ax0.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax0.spines.itervalues()]

ax2.set_ylim([1.6,4.2])
ax2.set_xlim([0.0,0.9])
ax2.set_ylabel(r'$E_{\rm form} (2+)$ (eV)')
ax2.plot(dat6[:,1], dat6[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax2.plot(dat6[:,1], dat6[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax2.plot(dat6[:,1], dat6[:,4],'s-', mec='g', color='g', zorder = 10)
ax2.plot(xx, func(xx,popt60[0], popt60[1], popt60[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax2.plot(xx, func(xx,popt61[0], popt61[1], popt61[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax2.plot(xx, popt60[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax2.yaxis.set_minor_locator(MultipleLocator(0.1))
ax2.xaxis.set_minor_locator(MultipleLocator(0.1))
ax2.xaxis.set_major_locator(MultipleLocator(0.2))
ax2.text(0.1, 0.85, r'R$_\mathregular{+}$, $\varepsilon_0$', transform=ax2.transAxes, fontsize=8)
ax2.tick_params('both', length=2, width=0.5, which='major')
ax2.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax2.spines.itervalues()]

ax3.set_ylim([0.8,3.4])
ax3.set_xlim([0.0,0.9])
#ax3.set_ylabel(r'$E_{\rm form} (2+)$ (eV)')
ax3.plot(dat5[:,1], dat5[:,2],'o', mec=crimson, color=crimson, zorder = 10)
ax3.plot(dat5[:,1], dat5[:,3],'o', mfc='w', mec=crimson, color=crimson, zorder = 9)
ax3.plot(dat5[:,1], dat5[:,4],'s-', mec='g', color='g', zorder = 10)
ax3.plot(xx, func(xx,popt50[0], popt50[1], popt50[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax3.plot(xx, func(xx,popt51[0], popt51[1], popt51[2]), '-', color=crimson, zorder = 1, lw = 0.8)
ax3.plot(xx, popt50[2]*ones, 'g:', zorder = 1, lw = 0.8)
ax3.yaxis.set_minor_locator(MultipleLocator(0.1))
ax3.xaxis.set_minor_locator(MultipleLocator(0.1))
ax3.xaxis.set_major_locator(MultipleLocator(0.2))
ax3.text(0.1, 0.85, r'R$_\mathregular{2+}$, $\varepsilon_0$', transform=ax3.transAxes, fontsize=8)
ax3.tick_params('both', length=2, width=0.5, which='major')
ax3.tick_params('both', length=1, width=0.5, which='minor')
[i.set_linewidth(0.5) for i in ax3.spines.itervalues()]

plt.tight_layout(pad=0.5)

plt.savefig('mgo_energy_conv.pdf')
plt.show()
